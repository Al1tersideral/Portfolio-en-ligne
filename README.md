# Portfolio des designers graphiques en ligne, limite et potentiel</br>

Master Thesis, EESAB-Rennes, 2022-2023</br>

Alain Maréchal</br>


This website explore the use of web medium as part of presenting graphic designer online.
By analysing multiple graphic designer from low/derivated use of the medium to an extended use of it.
I understand how they take a grasp of it and how they want it to be representative of their own practice.

Be free to download this website and read it or go to https://memoire.alainmarechal.xyz/ to check it out online

Website done in HTML, CSS, JS
