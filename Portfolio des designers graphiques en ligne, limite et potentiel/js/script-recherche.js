let recherches = [
  ['Laurenz Brunner','images/recherche/low/mp4/brunner.mp4', 'http://laurenzbrunner.com/','images/brunner.png'],
  ['Charles Mazé & Coline Sunier','images/recherche/low/mp4/cscm2.mp4', 'http://maze-sunier.cc/','images/cscm.png'],
  ['Daniel Eatock','images/recherche/low/mp4/eatock.mp4', 'https://eatock.com/','images/eatock.png'],
  ['Experimental Jet Set','images/recherche/low/mp4/jetset.mp4', 'https://www.jetset.nl/','images/jetset.png'],
  ['Teschner-Sturacci','images/recherche/low/mp4/teschner.mp4', 'http://teschner-sturacci.com/','images/teschner.png'],
  ['Cyan Studio','images/recherche/low/mp4/cyan.mp4', 'https://www.cyan.de/','images/cyan.png'],
  ['Dia Studio','images/recherche/low/mp4/dia.mp4', 'https://dia.tv/','images/dia.png'],
  ['Studio Push','images/recherche/low/mp4/push.mp4', 'https://studio-push.com/','images/push.png'],
  ['Mire Studio','images/recherche/low/mp4/mire.mp4', 'https://mire.studio/','images/mire.png'],
  ['Liebermann Kiepe Reddemann','images/recherche/low/mp4/zach.mp4', 'https://liebermannkiepereddemann.de/','images/liebermann.png'],
  ['AAJdesign','images/recherche/low/mp4/aajdesign.mp4', 'https://www.aajdesign.com/','images/recherche/low/posters/aajdesign.png'],
  ['Studio Abbasi','images/recherche/low/mp4/abbasi.mp4', 'http://www.studioabbasi.com/Works.aspx','images/recherche/low/posters/abbasi.png'],
  ['alessandri design & marken manufaktur','images/recherche/low/mp4/alessandri-design.mp4', 'https://www.alessandri-design.at/en/','images/recherche/low/posters/alessandri-design.png'],
  ['Alvin-Chan','images/recherche/low/mp4/alvin-chan.mp4', 'http://www.alvinchan.nl/','images/recherche/low/posters/alvin-chan.png'],
  ['Anette Lenz','images/recherche/low/mp4/annete-lenz.mp4', 'https://www.anettelenz.com/','images/recherche/low/posters/annete-lenz.png'],
  ['Atelier Bundi','images/recherche/low/mp4/atelier-bundi.mp4', 'https://atelierbundi.ch/en/','images/recherche/low/posters/atelier-bundi.png'],
  ['Atelier ter Bekke & Behage','images/recherche/low/mp4/atelier-ter-bekke.mp4', 'http://www.terbekke-behage.com/','images/recherche/low/posters/atelier-ter-bekke.png'],
  ['B-Design & IMAGRAM Design','images/recherche/low/mp4/b-design.mp4', 'http://imagram.com/','images/recherche/low/posters/b-design.png'],
  ['benoit bonnemaison-fitte','images/recherche/low/mp4/benoit-bonnemaison-fitte.mp4', 'https://bonnefrite.cheap/','images/recherche/low/posters/bennoit-bonnemaison-fitte.png'],
  ['adigard+mcshane_M-A-D','.images/recherche/low/mp4/berkley-ca.mp4', 'http://m-a-d.com/','images/recherche/low/posters/berkeley-ca.png'],
  ['Bibliothèque','images/recherche/low/mp4/bibliothequedesign.mp4', 'https://bibliothequedesign.com/','images/recherche/low/posters/bibliothequedesign.png'],
  ['Bohatsch und Partner','images/recherche/low/mp4/bohatsch-und-partner.mp4', 'http://www.bohatschundpartner.at/en/','images/recherche/low/posters/bohatsch-und-partner.png'],
  ['Change is good','images/recherche/low/mp4/changeisgood.mp4', 'http://www.changeisgood.fr/','images/recherche/low/posters/changeisgood.png'],
  ['The Designers Republic™','images/recherche/low/mp4/designer-republic.mp4', 'https://www.thedesignersrepublic.com/','images/recherche/low/posters/designer-republic.png'],
  ['Erich Brechbühl','images/recherche/low/mp4/erich-brechbul.mp4', 'https://erichbrechbuhl.ch/','images/recherche/low/posters/erich-brechbul.png'],
  ['Erik Brandt','images/recherche/low/mp4/erik-brandt.mp4', 'https://www.typografika.com/','images/recherche/low/posters/erik-brandt.png'],
  ['A Practice for Everyday Life','images/recherche/low/mp4/everydaylife.mp4', 'https://www.apracticeforeverydaylife.com/','images/recherche/low/posters/everydaylife.png'],
  ['Michel Bouvet','images/recherche/low/michel-bouvet.mp4', 'https://michelbouvet.com/','images/recherche/low/posters/michel-bouvet.png'],
  ['M/M','images/recherche/low/mp4/mmparis.mp4', 'http://www.mmparis.com/','images/recherche/low/posters/mmparis.png'],
  ['Mucho','images/recherche/low/mp4/mucho.mp4', 'https://wearemucho.com/','images/recherche/low/posters/mucho.png'],
  ['Nicholas Blechman','images/recherche/low/mp4/nicholas-blechman.mp4', 'https://www.nicholasblechman.com/','images/recherche/low/posters/nicholas-blechman.png'],
  ['Nick Bell Design','images/recherche/low/mp4/nick-bell.mp4', 'https://www.nickbelldesign.co.uk/','images/recherche/low/posters/nick-bell.png'],
  ['Pentagram','images/recherche/low/mp4/pentagram.mp4', 'https://www.pentagram.com/','images/recherche/low/posters/pentagram.png'],
  ['Peter Bankov','images/recherche/low/mp4/peter-bankov.mp4', 'https://bankovposters.com/','images/recherche/low/posters/peter-bankov.png'],
  ['Philippe Apeloig','images/recherche/low/mp4/philippe-apeloig.mp4', 'https://apeloig.com/','images/recherche/low/posters/philippe-apeloig.png'],
  ['Rice','images/recherche/low/mp4/rice.mp4', 'https://thisisri.mp4/','images/recherche/low/posters/rice.png'],
  ['Seymour Chwast','images/recherche/low/mp4/seymour-chwast.mp4', 'http://www.pushpininc.com/','images/recherche/low/posters/seymour-chwast.png'],
  ['ahn sang-soo','images/recherche/low/mp4/ssahn.mp4', 'http://ssahn.com/','images/recherche/low/posters/ssahn.png'],
]


var grid = document.querySelector('.grid');

recherches.forEach(recherche => {

  document.querySelector('.grid').innerHTML += `
  <div class="grid-item">
    <div class="info">
      <a href="${recherche[2]}">
      <p class="names">
      ${recherche[0]}
      </p>
      </a>
    </div>
    <section class="main">
    <video class="lazy" preload="none" data-poster="${recherche[3]}" muted loop>
    <source data-src="${recherche[1]}" type="video/mp4">
    </video>
    <div class="spinner" id="spinner">
    </div>
    </section>
  </div>`;
  

});

    var lazyLoadInstance = new LazyLoad({
  // Your custom settings go here
});
lazyLoadInstance.update();


window.onscroll = function () {
  scrollFunction()
};

function scrollFunction() {
  if (document.body.scrollTop > 250 || document.documentElement.scrollTop > 250) {
    document.getElementById("titre-memoire").style.fontSize = "1.3em";
  } else {
    document.getElementById("titre-memoire").style.fontSize = "2.4em";
  }
}

var msnry = new Masonry( '.grid', {
  columnWidth: '.grid-sizer',
  percentPosition: true,
  itemSelector: '.grid-item',
  gutter:5,
});

var elements = document.querySelectorAll('.grid-item');

elements.forEach(element => {

  element.addEventListener('click', function(){

    elements.forEach(element => {
      element.classList.remove('grid-item--2');
    });

    if (element.className.match('grid-item--2')) {
      element.classList.remove('grid-item--2');
    }
    else{
      element.classList.add('grid-item--2');
    }

    msnry.layout();

  });

  var lesvideos = document.querySelectorAll('video');

  lesvideos.forEach(lavideo => {
    lavideo.addEventListener('mouseover', function(){
      lavideo.play();
    });
    lavideo.addEventListener('mouseleave', function(){
      lavideo.pause();
    });
  });


});
