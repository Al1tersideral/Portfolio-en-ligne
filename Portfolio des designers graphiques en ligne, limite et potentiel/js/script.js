var lazyLoadInstance = new LazyLoad({
  // Your custom settings go here
});

window.onscroll = function () {
  scrollFunction()
};

function scrollFunction() {
  if (document.body.scrollTop > 250 || document.documentElement.scrollTop > 250) {
    document.getElementById("titre-memoire").style.fontSize = "1.3em";
    document.getElementById("note").style.opacity = "1";
    document.querySelector('.summary').style.opacity = "1";
    document.querySelector('.videoHeader').style.display ="none";
  } else {
    document.getElementById("titre-memoire").style.fontSize = "2.4em";
    document.getElementById("note").style.opacity = "0";
    document.querySelector('.summary').style.opacity = "0";
    document.querySelector('.videoHeader').style.display ="block";
  }
}

var slider = document.querySelector(".slider");
var paragraphes = document.querySelector('.contenu');

function createCookie(name, value, days) {
  var expires

  if (days) {
    var date = new Date()
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000)
    expires = '; expires=' + date.toGMTString()
  } else {
    expires = ''
  }

  document.cookie = name + '=' + value + expires + '; path=/'
}

function readCookie(name) {
  var nameEQ = name + '='
  var ca = document.cookie.split(';')

  for (var i = 0; i < ca.length; i++) {
    var c = ca[i]
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length)
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length)
    }
  }

  return null
}

function eraseCookie(name) {
  createCookie(name, '', -1)
}

var sizeFont = readCookie('taille');
paragraphes.style.fontSize = sizeFont+'px';
slider.value=sizeFont;

slider.addEventListener('input', function() {
  var size = slider.value
  paragraphes.style.fontSize = size +'px';
  createCookie('taille', size, 30);
} );



//Query tous les élements devant apparaitrent au moment 
//ou le titre de la partie arrive en haut de la page
var videos = document.querySelectorAll('video');
var titres = document.querySelectorAll('.memoire');
var notes = document.querySelectorAll('.note');
var courants = document.querySelectorAll('.courant');
var grille = document.getElementById('dark');

//Au Scroll
window.addEventListener('scroll', function (event) {
  //Pour chaque titre de partie
  titres.forEach(titre => {
    var positionTop = titre.getBoundingClientRect().top;
    //si la position du titre est a 200px du haut de la page
    if (positionTop <= 200 && positionTop >=-900) {
      

      var idVideo = titre.id;

      //Pour chaque video
      videos.forEach(lavideo => {
        lavideo.addEventListener('mouseover', function(){
          grille.classList.add('focus');
        });
        lavideo.addEventListener('mouseleave', function(){
          grille.classList.remove('focus');
        });

        var classVideo = lavideo.classList[0];

        lavideo.classList.remove('visible');
        //Si la vidéo à le même id que le titre alors ajouter class visible
        if (idVideo == classVideo && !lavideo.classList.contains('visible')) {
          lavideo.classList.add('visible');
          lavideo.play();
        }
        
        notes.forEach(note => {
          //Si la note à le même id que le titre alors ajouter class visible
          note.classList.remove('visible');
          var noteId = note.firstElementChild.className;
          if (idVideo == noteId) {
            note.classList.add('visible');
          }
        });
        courants.forEach(courant => {
          courant.classList.remove('visible');
          var courantId = courant.firstElementChild.className;
          if(idVideo == courantId){
            courant.classList.add('visible');
          }
        }); 
      });
    }
  });
}, false);

var titrage = document.getElementById('acceuil');
  titrage.addEventListener('click', function(){

    videos.forEach(lavideo => {
      lavideo.classList.remove('visible');

      notes.forEach(note => {
        note.classList.remove('visible');
      });
      courants.forEach(courant=> {
        courant.classList.remove('visible');
      });

    });
});


document.querySelectorAll('.summary a[href^="#"]').forEach(trigger => {
        trigger.onclick = function(e) {
            e.preventDefault();
            let hash = this.getAttribute('href');
            let target = document.querySelector(hash);
            let headerOffset = 80;
            let elementPosition = target.offsetTop;
            let offsetPosition = elementPosition - headerOffset;

            window.scrollTo({
                top: offsetPosition,
                behavior: "smooth"
            });
        };
    });
document.querySelectorAll('.sommaire a[href^="#"]').forEach(trigger => {
            trigger.onclick = function(e) {
                e.preventDefault();
                let hash = this.getAttribute('href');
                let target = document.querySelector(hash);
                let headerOffset = 80;
                let elementPosition = target.offsetTop;
                let offsetPosition = elementPosition - headerOffset;
    
                window.scrollTo({
                    top: offsetPosition,
                    behavior: "smooth"
                });
            };
    });

var zooms = document.querySelectorAll('.zoom');
var extraits = document.querySelector('.extrait').childNodes;
var box = document.querySelector('.extrait');

zooms.forEach(zoom => {
  var zoomId = zoom.id;

  zoom.addEventListener('mouseover', function(){

    extraits.forEach(extrait => {

      var extraitId = extrait.className;

      if(zoomId == extraitId){
        box.classList.add('visible');
        extrait.classList.add('visible');
      }
      zoom.addEventListener('mouseleave', function(){
        box.classList.remove('visible');
        extrait.classList.remove('visible');
  
      });
    });

  });
});

